import React from "react";
import moment from "moment";
import styles from "./styles.module.scss";

const chatHeader = ({ messages }) => {
  const messageCount = messages.length;
  const userCount = new Set(messages.map((message) => message.user)).size;
  const lastMessage = messages[messageCount - 1]?.createdAt;
  const lastMessageTime = moment(lastMessage).format("LT");
  return (
    <div className={styles.chatHeader}>
      <div className="">My Chat</div>
      <div className="">{`${userCount} participants`}</div>
      <div className="">{`${messageCount} messages`}</div>
      <div className="">{`Last message at ${lastMessageTime}`}</div>
    </div>
  );
};

export default chatHeader;
