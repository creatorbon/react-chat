import React, { useState, useEffect } from "react";
import styles from "./styles.module.scss";

const MessageInput = ({ inputRef, isEdit, sendMessage }) => {
  const [value, setValue] = useState("");
  const { status, message } = isEdit;

  useEffect(() => {
    if (status) {
      setValue(message.text);
    }
  }, [message.text, status]);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const submitCombination = (event) => {
    if (event.ctrlKey && event.keyCode === 13 && value.trim()) {
      sendMessage(value);
      setValue("");
    }
  };

  const submit = () => {
    if (value.trim()) {
      sendMessage(value);
      setValue("");
    }
  };

  return (
    <div className={styles.messageInput} onKeyDown={submitCombination}>
      <textarea
        ref={inputRef}
        placeholder="Введите текст сообщения…"
        value={value}
        onChange={handleChange}
      />
      <button onClick={submit}>
        <i className="fas fa-play fa-lg"></i>
      </button>
    </div>
  );
};

export default MessageInput;
