import React from "react";
import Message from "../Message";
import styles from "./styles.module.scss";
import Spinner from "../Spinner";
import TimeLine from "../TimeLine";
import * as groupBy from "lodash/groupBy";

const MessageList = ({
  blockRef,
  user,
  messages,
  error,
  isLoaded,
  onDelete,
  onEdit,
  onLike,
}) => {
  const groupedData = groupBy(messages, function (message) {
    return message.createdAt.split("T")[0];
  });

  const renderGroupedData = (groups) =>
    Object.keys(groups).map((group) => {
      return (
        <React.Fragment key={group}>
          <TimeLine data={group} />
          {groups[group].map((message) => (
            <Message
              key={message.id}
              isMe={user.userId === message.userId}
              time={message.createdAt}
              message={message}
              onDelete={onDelete}
              onEdit={onEdit}
              onLike={onLike}
            />
          ))}
        </React.Fragment>
      );
    });

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return (
      <div className={styles.messageList}>
        <Spinner />
      </div>
    );
  } else {
    return (
      <div ref={blockRef} className={styles.messageList}>
        {renderGroupedData(groupedData)}
      </div>
    );
  }
};

export default MessageList;
