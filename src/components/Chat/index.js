import React, { useEffect, useState, useRef } from "react";
import { v4 as uuid } from "uuid";
import moment from "moment";
import MessageInput from "../MessageInput";
import MessageList from "../MessageList";
import ChatHeader from "../ChatHeader";

const Chat = () => {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [messages, setMessages] = useState([]);
  const [isEdit, setIsEdit] = useState({ status: false, message: {} });
  const user = {
    user: "Ben",
    avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
    userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
  };

  const messagesRef = useRef(null);
  const inputRef = useRef(null);

  useEffect(() => {
    fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setMessages(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  useEffect(() => {
    if (!messagesRef.current) {
      return;
    } else {
      messagesRef.current.scrollTo(0, 999999);
    }

    inputRef.current.focus();
  }, [messagesRef, messages]);

  const handleLike = (id) => {
    const index = messages.findIndex((message) => message.id === id);
    const updateMessages = [...messages];
    const currentLikes = updateMessages[index].likes || [];
    const userLikeIndex = currentLikes.indexOf(user.userId);
    if (userLikeIndex === -1) {
      updateMessages[index] = {
        ...messages[index],
        likes: [...currentLikes, user.userId],
      };
    } else {
      currentLikes.splice(userLikeIndex, 1);
      updateMessages[index] = { ...messages[index], likes: [...currentLikes] };
    }

    setMessages(updateMessages);
  };

  const handleMessage = (value) => {
    if (isEdit.status) {
      const index = messages.findIndex(
        (message) => message.id === isEdit.message.id
      );
      const updateMessages = [...messages];
      updateMessages[index] = {
        ...messages[index],
        editedAt: moment().format(),
        text: value,
      };
      setMessages(updateMessages);
      setIsEdit({ status: false, message: {} });
    } else {
      setMessages([
        ...messages,
        {
          id: uuid(),
          text: value,
          user: user.user,
          avatar: user.avatar,
          userId: user.userId,
          editedAt: "",
          createdAt: moment().format(),
        },
      ]);
    }
  };

  const handleDelete = (id) => {
    const updateMessages = messages.filter((message) => message.id !== id);
    setMessages(updateMessages);
  };

  const handleEdit = (id) => {
    const index = messages.findIndex((message) => message.id === id);
    setIsEdit({ status: true, message: messages[index] });
    inputRef.current.focus();
  };

  return (
    <>
      <ChatHeader messages={messages}></ChatHeader>
      <MessageList
        blockRef={messagesRef}
        user={user}
        messages={messages}
        error={error}
        isLoaded={isLoaded}
        onDelete={handleDelete}
        onEdit={handleEdit}
        onLike={handleLike}
      ></MessageList>
      <MessageInput
        inputRef={inputRef}
        sendMessage={handleMessage}
        isEdit={isEdit}
      ></MessageInput>
    </>
  );
};

export default Chat;
