import React from "react";
import "./App.scss";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Chat from "./components/Chat";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Chat></Chat>
      <Footer></Footer>
    </div>
  );
}

export default App;
